Thead-Scheduler
===============

A basic C program that schedules threads created by main. The scheduler uses a cooperative round-robin system to switch between threads.

In main, we create a linked-list, which will contain the all the threads. We then call thread_create() which takes in a function, sets up the thread with a stack, and then returns a thread object. The thread must then be put into the thread linked-list. After the thread has been placed in the linked-list, one can call thread_start_threading(), which will start with the first scheduled thread.

Context switching was probably the toughest bug. We had a hard time getting the size just right for the stack for each thread. We kept getting seg faults. After fixing the seg fault problem, which was just making the stack size 4096, the program swiched between threads with a little assembly, which would save/load the registers. 
